# [React Practice](https://stanleyjh.gitlab.io/learn-react/practice)

### Task List App

Display a list of tasks with the ability to add and remove tasks.

- setting up Redux
- create actions and reducers
- connect components to the Redux store.
