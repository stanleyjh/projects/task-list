import { ADD_TASK, REMOVE_TASK } from "./action/actionTypes";
import { v4 } from "uuid";
import { TaskState } from "../types/taskState";

const initialState: TaskState = {
  numOfTasks: 0,
  // suggestion DONE - change it object OR array of objects. Also use UUID for key.
  tasks: {},
};

export default function reducer(
  state: { numOfTasks: number; tasks: any } = initialState,
  action: { type: string; payload: string }
) {
  switch (action.type) {
    case ADD_TASK:
      state.tasks[v4()] = action.payload;
      return {
        ...state,
        numOfTasks: state.numOfTasks + 1,
      };
    case REMOVE_TASK:
      delete state.tasks[action.payload];
      return {
        ...state,
        numOfTasks: state.numOfTasks - 1,
      };
    default:
      return state;
  }
}
