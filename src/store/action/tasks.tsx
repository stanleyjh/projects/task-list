import { ADD_TASK, REMOVE_TASK } from "./actionTypes";

// action creator
const addTaskAction = (task: string) => {
  return {
    type: ADD_TASK,
    payload: task,
  };
};

const removeTaskAction = (uuid: string) => {
  return {
    type: REMOVE_TASK,
    payload: uuid,
  };
};

export { addTaskAction, removeTaskAction };
