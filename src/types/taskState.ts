export interface TaskState {
  numOfTasks: number;
  tasks: {};
}

export interface ActionState {
  type: string;
  payload: {};
}
