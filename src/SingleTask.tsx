import { Dispatch } from "redux"
import { removeTaskAction } from "./store/action/tasks"
import { connect } from "react-redux"
import { Paper, Typography, Button } from "@mui/material"
import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded"

function SingleTask({ task, removeTask }: { task: any; removeTask: any }) {
  return (
    <Paper
      sx={{
        m: 1,
        pl: 2,
        pr: 2,
        pt: 1,
        pb: 1,
        width: "80%",
        display: "flex",
        justifyContent: "space-between",
      }}
      elevation={2}
    >
      <Typography
        component="span"
        sx={{ display: "flex", alignItems: "center" }}
      >
        {task[1]}
      </Typography>
      <Button
        sx={{ color: "red" }}
        onClick={() => {
          removeTask(task[0])
        }}
      >
        <DeleteRoundedIcon />
      </Button>
    </Paper>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    removeTask: (uuid: string) => dispatch(removeTaskAction(uuid)),
  }
}

export default connect(null, mapDispatchToProps)(SingleTask)
