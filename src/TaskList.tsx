import SingleTask from "./SingleTask"
import { Dispatch } from "redux"
import { useState } from "react"
import { connect } from "react-redux"
import { addTaskAction } from "./store/action/tasks"
import { TaskState } from "./types/taskState"
import {
  Container,
  Box,
  Typography,
  Avatar,
  TextField,
  Button,
} from "@mui/material"
import AssignmentIcon from "@mui/icons-material/Assignment"

interface IProps {
  addTask: (task: string) => { type: string; payload: string }
  numOfTasks: number
  tasks: {}
}

interface IStateProps {
  numOfTasks: number
  tasks: {}
}

const TaskList = ({ addTask, numOfTasks, tasks }: IProps) => {
  // typescript: Type Assertion
  const selectInput = document.getElementById("input") as HTMLInputElement
  const [task, setTask] = useState("")

  return (
    <Container maxWidth="sm">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ backgroundColor: "#5656ff" }}>
          <AssignmentIcon />
        </Avatar>
        <Typography variant="h1" sx={{ fontSize: 40, m: 1 }}>
          Task List
        </Typography>
        <Box component="section" sx={{ m: 2 }}>
          <TextField
            id="input"
            name="task"
            placeholder="Enter Task"
            onChange={(event) => setTask(event.target.value)}
          ></TextField>
          <Button
            sx={{ m: 1 }}
            variant="contained"
            onClick={() => {
              if (selectInput.value) {
                addTask(task)
                // clear input
                selectInput.value = ""
              } else {
                alert("Please enter a task.")
              }
            }}
          >
            Add Task
          </Button>
        </Box>
        {Object.entries(tasks).map((task) => {
          return <SingleTask key={task[0]} task={task} />
        })}
        <Typography component="h6">
          Total tasks: <span>{numOfTasks}</span>
        </Typography>
      </Box>
    </Container>
  )
}

const mapStateToProps = (state: TaskState): IStateProps => {
  return {
    numOfTasks: state.numOfTasks,
    tasks: state.tasks,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    addTask: (task: string) => dispatch(addTaskAction(task)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
